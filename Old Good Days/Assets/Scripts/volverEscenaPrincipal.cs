using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class volverEscenaPrincipal : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("EscenaPrincipal");

            if (Input.GetKeyDown(KeyCode.R))
            {
                RecargarEscenaActual();
            }
        }
    }
    private void RecargarEscenaActual()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
