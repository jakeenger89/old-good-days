using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorSoga : MonoBehaviour
{
    public float jumpForce = 5.0f;
    public float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;
    int contadorSaltos = 0;
    public string nombreJugador;

    private Rigidbody rb;
    private bool isGrounded;

    public KeyCode teclaSalto;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        isGrounded = Physics.CheckSphere(transform.position, groundCheckRadius, groundLayer);

        GameManager.Ganador = nombreJugador;

        if (isGrounded && Input.GetKeyDown(teclaSalto))
        {
            if (contadorSaltos > 0)
            {
                Jump();
                contadorSaltos--;
            }
        }
    }
    private void Jump()
    {
        rb.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
    }
    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Soga")
        {
            GameManager.jugadoresMuertos++;
            Destroy(this.gameObject);
        }
        else if (col.transform.CompareTag("piso"))
        {
            contadorSaltos = 1;
        }
    }
}