using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soga : MonoBehaviour
{
    public float velocidadInicial = 1f;
    public float aumentoVelocidad = 0.1f;
    public float velocidadMaxima = 5f;

    private float velocidadActual;

    private void Start()
    {
        velocidadActual = velocidadInicial;
    }

    private void Update()
    {
        velocidadActual += aumentoVelocidad * Time.deltaTime;
        velocidadActual = Mathf.Clamp(velocidadActual, 0f, velocidadMaxima);

        transform.Rotate(velocidadActual * Time.deltaTime, 0f, 0f);
    }
}