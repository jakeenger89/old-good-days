using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverEnCirculo : MonoBehaviour
{
    public float radio = 5f;
    public float velocidad = 2f;

    public string nombreJugador;

    public float angulo;
    private Vector3 centro;
    public Transform objetoCentro;

    public KeyCode teclaSentarse;

    public int jugadoresSentados;
    public static bool puedeSentarse;
    public bool estaDentroDeSilla;

    private GameObject gameobjectColision;
    private silla sillaColisionada; // Referencia al script "silla"

    public static bool cambioDeNivel;

    private List<silla> sillas; // Lista de sillas en la escena

    private void Start()
    {
        centro = objetoCentro.transform.position;
        GameManagerSilla.etapa = 1;

        // Obtener todas las sillas en la escena
        sillas = new List<silla>(FindObjectsOfType<silla>());
    }

    private void Update()
    {
        if (this.gameObject.tag != "sentado")
        {
            float x = centro.x + Mathf.Sin(angulo) * radio;
            float y = centro.y;
            float z = centro.z + Mathf.Cos(angulo) * radio;

            transform.position = new Vector3(x, y, z);

            angulo += velocidad * Time.deltaTime;

            if (angulo >= 360f)
            {
                angulo = 0f;
            }

            if (Input.GetKeyDown(teclaSentarse))
            {
                if (estaDentroDeSilla && sillaColisionada != null && sillaColisionada.isAvailable)
                {
                    jugadoresSentados++;
                    transform.position = gameobjectColision.transform.position;
                    this.gameObject.transform.tag = "sentado";
                    sillaColisionada.isAvailable = false;
                    GameManagerSilla.jugadoresSentados++;
                    GameManagerSilla.sillasDisponibles--;

                    if (jugadoresSentados == 3)
                    {
                        // Desaparecer jugador restante
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                // Reiniciar
            }
        }

        // Comprobar si hay un cambio de nivel
        if (cambioDeNivel)
        {
            // Hacer que todas las sillas est�n disponibles
            foreach (silla s in sillas)
            {
                s.isAvailable = true;
            }

            cambioDeNivel = false; // Reiniciar la variable de cambio de nivel
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (puedeSentarse && other.CompareTag("silla"))
        {
            estaDentroDeSilla = true;
            gameobjectColision = other.gameObject;
            sillaColisionada = other.GetComponent<silla>(); // Obtener referencia al script "silla"
            if (sillaColisionada != null)
            {
                // Hacer algo con la variable "isAvailable"
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        gameobjectColision = null;
        estaDentroDeSilla = false;
        sillaColisionada = null; // Reiniciar la referencia al script "silla"
    }
}
