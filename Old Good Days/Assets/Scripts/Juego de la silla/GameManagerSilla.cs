using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerSilla : MonoBehaviour
{
    public static int jugadoresSentados;
    public static int sillasDisponibles;
    public Camera camaraFin;

    public static string nombreGanador;

    public static int etapa;

    private bool finJuego;

    public List<GameObject> sillasEscena;

    private void Start()
    {
        camaraFin.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (etapa == 1)
        {
            sillasDisponibles = 3;
        }
        else if (etapa == 2)
        {
            sillasDisponibles = 2;
        }
        else if (etapa == 3)
        {
            sillasDisponibles = 1;
        }

        if (jugadoresSentados == sillasDisponibles)
        {
            etapa++;
            if (etapa > 3)
            {
                finJuego = true;
            }
            else
            {
                if (sillasEscena.Count > 0)
                {
                    GameObject sillaEliminada = sillasEscena[0];
                    sillasEscena.RemoveAt(0);
                    Destroy(sillaEliminada);
                }

                GameObject jugador = GameObject.FindGameObjectWithTag("Player");
                if (jugador != null)
                {
                    Destroy(jugador);
                }

                GameObject[] sentados = GameObject.FindGameObjectsWithTag("sentado");
                foreach (GameObject sentado in sentados)
                {
                    sentado.tag = "Player";
                    MoverEnCirculo.cambioDeNivel = true;
                    ReproducirCancionAleatoria.reproducir = true;
                }
            }

            StartCoroutine(ResetCambioDeNivel());
        }

        GameObject[] sillas = GameObject.FindGameObjectsWithTag("silla");

        foreach (GameObject silla in sillas)
        {
            if (!sillasEscena.Contains(silla))
            {
                sillasEscena.Add(silla);
            }
        }
        Debug.Log(finJuego);

        if (finJuego == true)
        {
            camaraFin.gameObject.SetActive(true);

        }
    }

    IEnumerator ResetCambioDeNivel()
    {
        yield return null;
        MoverEnCirculo.cambioDeNivel = false;
        jugadoresSentados = 0;
    }
}
