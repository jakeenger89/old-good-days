using UnityEngine;

public class CanvasDisabler : MonoBehaviour
{
    public Canvas canvas;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            canvas.enabled = false;
        }
    }
}