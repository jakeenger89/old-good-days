using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicaSillas : MonoBehaviour
{
    public AudioClip collisionSound;

    private AudioSource audioSource;
    private bool isPlaying = false;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = collisionSound;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !isPlaying)
        {
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
            else
            {
                audioSource.UnPause();
            }

            isPlaying = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && isPlaying)
        {
            audioSource.Pause();
            isPlaying = false;
        }
    }
}
