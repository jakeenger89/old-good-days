using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CamaraPodio : MonoBehaviour
{
    public TextMeshProUGUI textoPodioUno;
    public TextMeshProUGUI textoPodioDos;
    public TextMeshProUGUI textoPodioTres;

    public void mostrarPodios()
    {
        textoPodioUno.text = ControlLlegada.podioUno;
        textoPodioDos.text = ControlLlegada.podioDos;
        textoPodioTres.text = ControlLlegada.podioTres;
    }
    private void Update()
    {
        mostrarPodios();
    }
}
