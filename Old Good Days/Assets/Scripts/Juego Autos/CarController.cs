using UnityEngine;

public class CarController : MonoBehaviour
{
    public float speed = 25f;
    public Transform[] waypoints;
    public int currentWaypoint = 0;
    private bool canMove = true;
    public bool ganar = false;
    public KeyCode teclaElegida;
    public string nombreJugador;

    private void Update()
    {
        if (Input.GetKeyDown(teclaElegida))
        {
            if (canMove)
            {
                MoverAuto();
                canMove = false;
            }
        }
        else if (Input.GetKeyUp(teclaElegida))
        {
            canMove = true;
        }
        if (currentWaypoint == 56)
        {
            ganar = true;
            ControlLlegada.ordenLlegada.Add(nombreJugador);
            Destroy(this);
        }
    }

    private void MoverAuto()
    {
        Vector3 targetPosition = waypoints[currentWaypoint].position;

        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

        if (Vector3.Distance(transform.position, targetPosition) < 0.5f)
        {
            currentWaypoint++;

            // Verificar si llegamos al final de los waypoints
            if (currentWaypoint >= waypoints.Length)
            {
                currentWaypoint = waypoints.Length - 1;
            }
        }
    }
}