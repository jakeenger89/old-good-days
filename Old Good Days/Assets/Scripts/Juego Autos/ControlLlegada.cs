using UnityEngine;
using System.Collections.Generic;

public class ControlLlegada : MonoBehaviour
{
    public static List<string> ordenLlegada = new List<string>();
    public static string podioUno;
    public static string podioDos;
    public static string podioTres;
    public GameObject camaraPodios;

    private void Update()
    {
        if (ordenLlegada.Count >= 3)
        {
            mostrarGanadores();
            camaraPodios.SetActive(true);
        }
    }

    private void Start()
    {
        camaraPodios.SetActive(false);
    }

    public void mostrarGanadores()
    {
        podioUno = ordenLlegada[0];
        podioDos = ordenLlegada[1];
        podioTres = ordenLlegada[2];
    }
}

