using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escudo : MonoBehaviour , IColeccionable
{
    public GameObject escudoGameObject;

    public void Recolectar(Transform jugadorTransform)
    {
        GameObject escudo = Instantiate(escudoGameObject, jugadorTransform.position, jugadorTransform.rotation);
        escudo.transform.parent = jugadorTransform;
        Destroy(escudo, 5f);
    }

}