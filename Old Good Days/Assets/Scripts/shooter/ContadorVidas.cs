using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContadorVidas : MonoBehaviour
{
    public static int vidas;
    public bool escudo = false; // Inicializamos el bool escudo en false

    private void Start()
    {
        vidas = 3;
    }

    private void Update()
    {
        if (vidas == 0)
        {
            Destroy(this.gameObject);
        }

        UpdateEscudoStatus();

        //if (this.gameObject.transform.CompareTag("Player 1")) // si es el jugador 1
        //{
        //    MostrarVidasJugadores.vidaJ1 = vidas;
        //}
        //else // si es el jugador 2
        //{
        //    MostrarVidasJugadores.vidaJ2 = vidas;
        //}
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("bala") || collision.transform.CompareTag("obstaculo"))
        {
            if (!escudo) 
            {
                vidas--;
            }
        }
    }

    private void UpdateEscudoStatus()
    {
        Transform escudoChild = transform.Find("Escudo(Clone)");
        escudo = (escudoChild != null);

        if (!escudo)
        {
            escudo = false;
        }
    }

    public void AumentarVidas(int cantidad)
    {
        vidas += cantidad;
        Debug.Log("Vidas actuales: " + vidas);
    }
}