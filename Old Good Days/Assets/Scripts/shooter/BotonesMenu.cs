using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BotonesMenu : MonoBehaviour
{
    public GameObject canvas;
    public GameObject menu;
    public GameObject pantallaJuego; //pantalla con las vidas de los jugadores
    public GameObject pantallaFinJuego;
    public bool finJuego;

    public void botonStart()
    {
        canvas.SetActive(true);
        pantallaJuego.SetActive(true);
        menu.gameObject.SetActive(false);
    }
    private void Start()
    {
        canvas.SetActive(false);
        pantallaJuego.SetActive(false);
        pantallaFinJuego.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ReloadScene();
        }
        if (finJuego)
        {
            pantallaFinJuego.SetActive(true);
        }
    }
    public void ReloadScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }
}