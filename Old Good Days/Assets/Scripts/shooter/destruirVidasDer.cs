using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destruirVidasDer : MonoBehaviour
{
    public GameObject vida1der;
    public GameObject vida2der;
    public GameObject vida3der;

    void Update()
    {
        if (ContadorVidas.vidas == 2)
        {
            Destroy(vida3der);
        }

        if (ContadorVidas.vidas == 1)
        {
            Destroy(vida2der);
        }

        if (ContadorVidas.vidas == 0)
        {
            Destroy(vida1der);
        }
    }
}
