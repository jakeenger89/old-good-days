using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorPrefab : MonoBehaviour
{
    public GameObject[] prefabs; // Lista de prefabs a generar.
    public int cantidadPrefabs = 10; // Cantidad de prefabs que quieres generar.
    public float spawnRadius = 10f; // Radio de generación.

    public float intervaloGeneracion = 5f; // Tiempo en segundos entre cada generación.

    private void Start()
    {
        InvokeRepeating("GenerarPrefabAleatorio", intervaloGeneracion, intervaloGeneracion);
    }

    private void GenerarPrefabAleatorio()
    {
        int randomIndex = Random.Range(0, prefabs.Length);
        Vector3 randomPosition = GetRandomPosition();
        Instantiate(prefabs[randomIndex], randomPosition, Quaternion.identity);
    }

    private Vector3 GetRandomPosition()
    {
        Vector3 randomDirection = Random.insideUnitSphere * spawnRadius;
        randomDirection.y = 0f; // Para que no se genere en altura.
        Vector3 randomPosition = transform.position + randomDirection;
        return randomPosition;
    }
}
