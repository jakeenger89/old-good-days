using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoJugador : MovimientoBase
{
    public Rigidbody rb;
    private MovimientoBase mbJ1;
    void FixedUpdate()
    {
        float horizontalInput = 0f;
        float verticalInput = 0f;

        if (Input.GetKey(KeyCode.A))
            horizontalInput = -1f;
        else if (Input.GetKey(KeyCode.D))
            horizontalInput = 1f;

        if (Input.GetKey(KeyCode.W))
            verticalInput = 1f;
        else if (Input.GetKey(KeyCode.S))
            verticalInput = -1f;

        Vector3 movement = new Vector3(horizontalInput, 0f, verticalInput) * speed * Time.deltaTime;

        rb.velocity = movement;

    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        mbJ1 = GetComponent<MovimientoBase>();

    }

    private void OnTriggerEnter(Collider other)
    {
        IColeccionable coleccionable = other.GetComponent<IColeccionable>();
        if (coleccionable != null)
        {
            coleccionable.Recolectar(transform);
            Destroy(other.gameObject);
        }
    }
       
    private void OnCollisionEnter(Collision collision)
    {
            iTnt tnt = collision.gameObject.GetComponent<iTnt>();
            if (tnt != null)
            {
                tnt.Destruir();
            }
        
    }
}
