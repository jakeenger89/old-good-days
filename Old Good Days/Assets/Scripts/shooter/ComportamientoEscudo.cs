using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoEscudo : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            Destroy(collision.gameObject);
        }
    }
}
